# EasyFEAT


EasyFEAT is a cut-down graphical interface to the [FSL
FEAT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT) task fMRI analysis
pipeline, intended for educational purposes.

![EasyFEAT interface](screenshot.png){width=50%}


The user needs to specify:
 - The 4D fMRI data
 - a brain-extracted structural image
 - Stimulus timing
 - Statistical tests (contrasts) to perform

EasyFEAT only supports block-based experimental designs, and only performs a
minimal set of pre-processing on the fMRI data:

 - High-pass filtering
 - Motion correction
 - Spatial smoothing
 - Linear registration from fMRI to structural to MNI152 template space.

Only a limited selection of contrasts can be specified:
 - Mean activation across all stimuli
 - Activation in response to one stimuli
 - Difference in activation between two stimuli

If three or more stimuli are present and the _Compare all_ checkbox is selected, an omnibus F-test is also performed, which will test for any difference in activation between any pair of stimuli.
