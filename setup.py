#!/usr/bin/env python

import os.path as op
from setuptools import setup

basedir = op.dirname(__file__)

with open(op.join(basedir, 'requirements.txt'), 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setup(
    name='easyfeat',
    version='0.3.0',
    description='easyfeat',
    url='https://git.fmrib.ox.ac.uk/fsl/easyfeat',
    author='Paul McCarthy',
    author_email='pauldmccarthy@gmail.com',
    packages=['easyfeat'],
    include_package_data=True,
    install_requires=install_requires,
    entry_points={
        'console_scripts' : [
            'easyfeat = easyfeat.main:main',
        ]
    }
)
