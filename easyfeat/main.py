#!/usr/bin/env python
#
# easyfeat.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import                operator
import                os
import                platform
import                sys
import                time
import functools   as ft
import itertools   as it
import os.path     as op
import subprocess  as sp

import numpy       as np
import                wx

import matplotlib.pyplot                 as plt
import matplotlib.backends.backend_wxagg as wxagg

from   fsl.utils.platform import platform as fslplatform
import fsl.data.image                     as fslimage


import fsleyes_widgets.widgetlist         as widgetlist
import fsleyes_widgets.utils.status       as status
import fsleyes_props                      as props


# Allow execution from non-framework pythons
# https://github.com/wxWidgets/Phoenix/issues/2250
# https://github.com/wxWidgets/Phoenix/pull/2252
if platform.system() == 'Darwin':
    import wx
    wx.PyApp.IsDisplayAvailable = lambda *_: True


DEFAULT_FSF_SETTINGS = {
    'version'                       : '6.00',
    'inmelodic'                     : '0',
    'level'                         : '1',
    'analysis'                      : '7',
    'relative_yn'                   : '0',
    'help_yn'                       : '0',
    'featwatcher_yn'                : '1',
    'sscleanup_yn'                  : '0',
    'ndelete'                       : '0',
    'tagfirst'                      : '1',
    'multiple'                      : '1',
    'inputtype'                     : '1',
    'filtering_yn'                  : '1',
    'brain_thresh'                  : '10',
    'critical_z'                    : '5.3',
    'noise'                         : '0.66',
    'noisear'                       : '0.34',
    'sh_yn'                         : '0',
    'regunwarp_yn'                  : '0',
    'gdc'                           : '""',
    'dwell'                         : '0.0',
    'te'                            : '0.0',
    'signallossthresh'              : '10',
    'unwarp_dir'                    : 'y-',
    'st'                            : '0',
    'st_file'                       : '""',
    'norm_yn'                       : '0',
    'perfsub_yn'                    : '0',
    'templp_yn'                     : '0',
    'melodic_yn'                    : '0',
    'stats_yn'                      : '1',
    'prewhiten_yn'                  : '1',
    'motionevs'                     : '0',
    'motionevsbeta'                 : '""',
    'scriptevsbeta'                 : '""',
    'robust_yn'                     : '0',
    'mixed_yn'                      : '0',
    'randomisePermutations'         : '5000',
    'constcol'                      : '0',
    'poststats_yn'                  : '1',
    'threshmask'                    : '""',
    'zdisplay'                      : '0',
    'zmin'                          : '2',
    'zmax'                          : '8',
    'rendertype'                    : '1',
    'bgimage'                       : '1',
    'tsplot_yn'                     : '1',
    'reginitial_highres_yn'         : '0',
    'reginitial_highres_search'     : '90',
    'reginitial_highres_dof'        : '3',
    'reghighres_yn'                 : '1',
    'reghighres_search'             : '90',
    'reghighres_dof'                : '6',
    'regstandard_yn'                : '1',
    'alternateReference_yn'         : '0',
    'regstandard_search'            : '90',
    'regstandard_nonlinear_yn'      : '0',
    'regstandard_nonlinear_warpres' : '10',
    'ncopeinputs'                   : '0',
    'alternative_mask'              : '""',
    'init_initial_highres'          : '""',
    'init_highres'                  : '""',
    'init_standard'                 : '""',
    'confoundevs'                   : '0',
    'overwrite_yn'                  : '0',
    'nftests_orig'                  : '0',
    'nftests_real'                  : '0',
    'evs_vox'                       : '0',
}


def generateFSF(feat, designfile):
    stddir = op.join(os.environ['FSLDIR'], 'data', 'standard')

    if feat.brainExtraction: standard = op.join(stddir, 'MNI152_T1_2mm_brain')
    else:                    standard = op.join(stddir, 'MNI152_T1_2mm')

    # General settings
    settings                    = dict(DEFAULT_FSF_SETTINGS)
    settings['outputdir']       = f'"{feat.outputDir}"'
    settings['tr']              = f'{feat.trTime}'
    settings['npts']            = f'{feat.numVolumes}'
    settings['mc']              = '1' if feat.motionCorrection else '0'
    settings['bet_yn']          = '1' if feat.brainExtraction  else '0'
    settings['temphp_yn']       = '1' if feat.highPass         else '0'
    settings['smooth']          = f'{feat.smoothingSize}'
    settings['paradigm_hp']     = f'{feat.highPassCutoff}'
    settings['thresh']          = f'{feat.thresholding}'
    settings['prob_thresh']     = f'{feat.pThreshold}'
    settings['z_thresh']        = f'{feat.zThreshold}'
    settings['regstandard']     = f'"{standard}"'
    settings['regstandard_dof'] = f'{feat.standardDOF}'
    settings['totalVoxels']     = f'{np.prod(feat.shape)}'

    # EVs
    evs                  = list(range(1, feat.nevs + 1))
    settings['evs_orig'] = f'{len(evs)}'
    settings['evs_real'] = f'{len(evs) * 2}'
    for ev in evs:
        name                            = feat.evnames[ ev - 1]
        on                              = feat.evons[   ev - 1]
        off                             = feat.evoffs[  ev - 1]
        phase                           = feat.evphases[ev - 1]
        settings[f'evtitle{ev}']        = f'"{name}"'
        settings[f'shape{ev}']          = '0'
        settings[f'convolve{ev}']       = '2'
        settings[f'convolve_phase{ev}'] = '0'
        settings[f'tempfilt_yn{ev}']    = '1'
        settings[f'deriv_yn{ev}']       = '1'
        settings[f'skip{ev}']           = '0'
        settings[f'off{ev}']            = f'{off}'
        settings[f'on{ev}']             = f'{on}'
        settings[f'phase{ev}']          = f'{phase}'
        settings[f'stop{ev}']           = '-1'
        settings[f'gammasigma{ev}']     = '3'
        settings[f'gammadelay{ev}']     = '6'

    # orthogonalisation of EVs w.r.t. each other
    # Dunno why we need ortho<n>.0
    for ev in evs:
        settings[f'ortho{ev}.0'] = '0'
    for ev1, ev2 in it.product(evs, evs):
        settings[f'ortho{ev1}.{ev2}'] = '0'

    # contrasts
    contrasts, connames = feat.contrasts
    settings['con_mode_old']          = 'orig'
    settings['con_mode']              = 'orig'
    settings['conmask_zerothresh_yn'] = '0'
    settings['ncon_orig']             = f'{len(contrasts)}'
    settings['ncon_real']             = f'{len(contrasts)}'

    for ci, (contrast, conname) in enumerate(zip(contrasts, connames), 1):
        settings[f'conpic_real.{ci}']  = '1'
        settings[f'conpic_orig.{ci}']  = '1'
        settings[f'conname_real.{ci}'] = f'"{conname}"'
        settings[f'conname_orig.{ci}'] = f'"{conname}"'

        # real contrast (including tderiv evs)
        for vi, cv in enumerate(contrast, 1):
            settings[f'con_real{ci}.{vi}'] = f'{cv}'

        # orig contrast (not including tderiv evs)
        for vi in range(1, feat.nevs + 1):
            settings[f'con_orig{ci}.{vi}'] = f'{contrast[(vi - 1) * 2]}'

    # contrast masking
    for ci, cj in it.product(range(len(contrasts)), range(len(contrasts))):
        settings[f'conmask{ci+1}_{cj+1}'] = '0'

    # f-test
    if feat.compareAll:
        fvec                     = feat.ftestVector
        settings['nftests_orig'] = '1'
        settings['nftests_real'] = '1'

        for i, val in enumerate(fvec):
            settings[f'ftest_real1.{i + 1}'] = str(val)
            settings[f'ftest_orig1.{i + 1}'] = str(val)

    # save to file
    with open(designfile, 'wt') as f:

        f.write(f'set feat_files(1) "{feat.inputFile}"\n')
        f.write(f'set highres_files(1) "{feat.structural}"\n')
        for k, v in settings.items():
            f.write(f'set fmri({k}) {v}\n')


class EasyFeat(props.HasProperties):

    # Data
    inputFile      = props.FilePath(exists=True,
                                    required=True,
                                    suffixes=fslimage.ALLOWED_EXTENSIONS)
    outputDir      = props.FilePath(required=True, isFile=False)
    numVolumes     = props.Int(minval=0,  clamped=True)
    trTime         = props.Real(minval=0, clamped=True)
    highPass       = props.Boolean(default=True)
    highPassCutoff = props.Real(minval=0, default=100, clamped=True)

    # Pre-stats
    motionCorrection = props.Boolean(default=True)
    brainExtraction  = props.Boolean(default=True)
    smoothing        = props.Boolean(default=True)
    smoothingSize    = props.Real(minval=0, default=5, clamped=True)

    # Registration - only up to 12 DOF linear reg supported
    structural  = props.FilePath(exists=True,
                                 required=True,
                                 suffixes=fslimage.ALLOWED_EXTENSIONS)
    standard    = props.FilePath(exists=True,
                                 required=True,
                                 suffixes=fslimage.ALLOWED_EXTENSIONS)
    standardDOF = props.Choice((3, 6, 7, 9, 12), default=12)

    # Stats - only square-wave EVs supported
    nevs     = props.Int(minval=1, maxval=6,  default=1, clamped=True)
    evnames  = props.List(props.String())
    evons    = props.List(props.Int(minval=0, maxval=60, default=15))
    evoffs   = props.List(props.Int(minval=0, maxval=60, default=45))
    evphases = props.List(props.Int(minval=0, maxval=60, default=0))

    # Add a contrast testing the mean across all EVs
    # (e.g. "1 1 1" for three EVs)
    meanContrast = props.Boolean(default=True)

    # Add a contrast for each individual EV
    # (e.g. "0 1 0" for EV#2)
    evContrasts = props.List(props.Boolean(default=True))

    # Add contrasts comparing evey EV with every other
    # EV (e.g. "1 -1 0" to compare EV1 with EV2). The
    # opposite test is always added. Example order for
    # three EVs is [(0, 1), (0, 2), (1, 2)]. See the
    # evpairs method.
    compareContrasts = props.List(props.Boolean(default=False))

    # Compare all pairs of EVs, and add an omnibus f-test
    # to compare for difference between any pair
    compareAll = props.Boolean(default=False)

    # Post-stats
    # The numeric values are directly used in the generated design.fsf
    thresholding = props.Choice(
        (0, 1, 2, 3),
        alternates=[['None'], ['Uncorrected'], ['Voxel'], ['Cluster']],
        default=3)

    zThreshold = props.Real(minval=0, default=3.1, clamped=True)
    pThreshold = props.Real(minval=0, maxval=1, default=0.05, clamped=True)


    def __init__(self):

        std = op.join(
            fslplatform.fsldir, 'data', 'standard', 'MNI152_T1_2mm_brain')

        try:              std = fslimage.addExt(std)
        except Exception: std = None

        self.standard = std

        self.listen('nevs',       str(id(self)), self.__nevsChanged)
        self.listen('inputFile',  str(id(self)), self.__inputImageChanged)
        self.listen('compareAll', str(id(self)), self.__compareAllChanged)

        self.__nevsChanged()


    def __nevsChanged(self):
        """Called when nevs is changed. Makes sure that evnames, evons, evoffs,
        and evphases all have the correct number of elements.
        """

        newnevs  = self.nevs
        oldnevs  = len(self.evnames)
        newpairs = newnevs * (newnevs - 1) // 2

        if oldnevs > newnevs:
            del self.evnames[         newnevs:]
            del self.evons[           newnevs:]
            del self.evoffs[          newnevs:]
            del self.evphases[        newnevs:]
            del self.evContrasts[     newnevs:]
            del self.compareContrasts[newpairs:]

        elif oldnevs < newnevs:
            nnew      = newnevs  - oldnevs
            nnewpairs = newpairs - (oldnevs * (oldnevs - 1) // 2)
            self.evnames         .extend([f'{i}' for i in range(oldnevs + 1,
                                                                newnevs + 1)])
            self.evons           .extend([15]    * nnew)
            self.evoffs          .extend([45]    * nnew)
            self.evphases        .extend([0]     * nnew)
            self.evContrasts     .extend([True]  * nnew)
            self.compareContrasts.extend([False] * nnewpairs)

        if newnevs <= 2:
            self.compareAll = False
        self.__compareAllChanged()


    @property
    def evpairs(self):
        """Return a list of tuples of EV pairs, ordered so as to match
        the compareContrasts list.
        """
        return list(it.combinations(range(self.nevs), 2))


    def __inputImageChanged(self):
        """Called when inputFile is changed. Tries to automatically find an
        associated structural image, and sets the FEAT output directory path.
        """

        try:
            image = fslimage.Image(self.inputFile)

        except Exception:
            self.inputFile  = None
            self.trTime     = 0
            self.numVolumes = 0
            return

        self.numVolumes = image.shape[ 3]
        self.trTime     = image.pixdim[3]

        basedir = op.dirname(self.inputFile)

        struc       = None
        possStructs = ['struct', 'struc', 'T1', 'structural']
        possStructs = possStructs + [ps.upper() for ps in possStructs]
        possStructs = [f'{ps}_brain' for ps in possStructs] + \
                      [f'{ps}_BRAIN' for ps in possStructs]

        for ps in possStructs:
            ps = op.join(basedir, ps)

            try:
                struc = fslimage.addExt(ps)
                break
            except Exception:
                struc = None

        if struc is not None:
            self.structural = struc

        if self.outputDir is None:
            self.outputDir = op.join(basedir, f'{image.name}.feat')


    def __compareAllChanged(self):
        """Called when the compareAll property changes. Sets and disables the
        individual comparison contrast flags.
        """

        comppvs = self.getProp('compareContrasts')
        for i in range(len(self.compareContrasts)):
            if self.compareAll: comppvs.disableItem(self, i)
            else:               comppvs.enableItem( self, i)

        if not self.compareAll:
            return

        for i, (evx, evy) in enumerate(self.evpairs):
            self.compareContrasts[i] = True


    @property
    def shape(self):
        if self.inputFile is not None:
            return fslimage.Image(self.inputFile).shape
        else:
            return None

    @property
    def contrasts(self):
        """Generate contrast vectors. """

        nevs      = self.nevs
        evnames   = self.evnames[:]
        connames  = []
        contrasts = []

        def gencontrast(contrast):

            if len(contrast) < nevs:
                contrast = contrast + [0] * (nevs - len(contrast))
            elif len(contrast) < nevs:
                contrast = contrast[:nevs]

            # add 0s for temporal derivative EVs
            tdevs    = [0] * nevs
            contrast = list(ft.reduce(operator.add, zip(contrast, tdevs)))

            return contrast

        # a mean contrast across all conditions
        if (nevs > 1) and self.meanContrast:
            convec = [1] * nevs
            contrasts.append(gencontrast(convec))
            connames .append('mean')

        # a contrast on each individual condition
        for i in range(nevs):
            if not self.evContrasts[i]:
                continue
            convec    = [0] * nevs
            convec[i] = 1
            connames .append(evnames[i])
            contrasts.append(gencontrast(convec))

        # Contrasts comparing pairs of EVs
        for i, (ev1, ev2) in enumerate(self.evpairs):
            if not self.compareContrasts[i]:
                continue

            poscon      = [0] * nevs
            negcon      = [0] * nevs
            poscon[ev1] =  1
            poscon[ev2] = -1
            negcon[ev1] = -1
            negcon[ev2] =  1

            contrasts.append(gencontrast(poscon))
            contrasts.append(gencontrast(negcon))
            connames .append(f'{evnames[ev1]} > {evnames[ev2]}')
            connames .append(f'{evnames[ev1]} < {evnames[ev2]}')

        return contrasts, connames


    @property
    def ftestVector(self):
        """Returns a vector defining the contrasts included in the f-test,
        if compareAll is enabled.
        """
        if not self.compareAll:
            raise RuntimeError()

        nevs     = self.nevs
        nevpairs = len(self.evpairs)

        # We're only interested in the comparison
        # contrasts, which are the 2*len(self.evpairs)
        # contrasts at the end of the list of contrasts

        connames, contrasts = self.contrasts
        ncontrasts          = len(contrasts)
        connames            = connames[ -2 * nevpairs:]
        contrasts           = contrasts[-2 * nevpairs:]

        # A contrast will have been added for all pairs
        # of EVs, but we only want to perform the
        # f-test over (ev<x>, ev<x+1>) pairs. If we
        # perform an f-test over all such pairs, the
        # results will be sensitive to a difference
        # between all pairs of EVs. Here we generate
        # (empirically derived) indices into the list
        # of contrasts.
        idxs = [0] + list(range(nevs - 1, 1, -1))
        idxs = 2 * np.cumsum(idxs)
        idxs = (ncontrasts - 2 * nevpairs) + idxs

        fvec = [0] * ncontrasts
        for i in idxs:
            fvec[i] = 1

        return fvec


    def isReady(self):

        if self.inputFile is None:
            raise RuntimeError('An input file needs to be specified!')

        if self.outputDir is None:
            raise RuntimeError('An output directory needs to be specified!')

        if self.structural is None:
            raise RuntimeError('You need to specify a structural image!')

        if self.standard is None:
            raise RuntimeError('You need to specify a standard space image!')

        # will raise RuntimeError if contrasts invalid
        self.contrasts


    def run(self):

        with status.reportIfError('FEAT setup error', 'You haven\'t finished '
                                  'setting up your FEAT analysis!'):
            self.isReady()

        if not self.outputDir.endswith('.feat'):
            self.outputDir = '{}.feat'.format(self.outputDir)

        with status.reportIfError('FEAT execution error', 'An error occurred '
                                  'when starting the FEAT analysis!'):
            generateFSF(self, 'design.fsf')
            fsldir = os.environ['FSLDIR']
            feat   = op.join(fsldir, 'bin', 'feat')
            sp.Popen([feat, 'design.fsf'], close_fds=True)
            time.sleep(1)


class ModelPanel(wx.Panel):
    """Visualises FEAT EVs on a matplotlib canvas. """
    def __init__(self, parent, feat):
        wx.Panel.__init__(self, parent)
        self.feat   = feat
        self.figure = plt.Figure()
        self.axis   = self.figure.add_subplot(111)
        self.canvas = wxagg.FigureCanvasWxAgg(self, -1, self.figure)
        self.figure.subplots_adjust(top=1.0, bottom=0.0, left=0.0, right=1.0)
        self.figure.patch.set_visible(False)

        for opt in ['inputFile', 'nevs', 'evnames',
                    'evons', 'evoffs', 'evphases']:
            self.feat.listen(opt, str(id(self)), self.refresh)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
        self.sizer.Add(self.canvas, flag=wx.EXPAND, proportion=1)

        self       .SetMinSize((-1, 150))
        self.canvas.SetMinSize((-1, 150))

        self.refresh()


    def refresh(self):

        feat      = self.feat
        canvas    = self.canvas
        axis      = self.axis
        nevs      = feat.nevs
        _, height = canvas.get_width_height()

        axis.clear()

        nsecs = feat.numVolumes * feat.trTime
        if nsecs == 0:
            axis.set_xlim((0.0, 1.0))
            axis.set_ylim((0.0, 1.0))
            axis.text(0.5, 0.5, 'Select an input file',
                      ha='center', va='center',
                      transform=axis.transAxes)
            return

        nsecs   = int(round(nsecs))
        seconds = np.arange(nsecs)

        for ev in range(nevs):
            on      = feat.evons[ev]
            off     = feat.evoffs[ev]
            phase   = feat.evphases[ev]
            wavelen = off + on

            # we invert the y axis below, so here 1=off, 0=on
            series               = np.ones(wavelen)
            series[off:off + on] = 0
            series               = np.roll(series, -phase)
            voffset              = ev * 1.5

            # TODO FEAT handles negative phase incorrectly -
            # it actually causes the start of the waveform
            # to be delayed. This is not yet implemented here

            nrepeats = int(np.ceil(len(seconds) / len(series)))
            series   = np.tile(series, nrepeats)[:len(seconds)]

            axis.step(seconds, series + voffset,
                      c=f'C{ev}', lw=2, where='post')

            if nevs > 1:
                combined_voffset = nevs * 1.5
                axis.step(seconds, series + combined_voffset, c=f'C{ev}',
                          lw=2, where='post', ls='--', alpha=0.5)

        xpad = seconds[-1] * 0.05
        axis.set_xlim((-1 - xpad, seconds[-1] + 1 + xpad))

        # invert yaxis so condition #1 is on top
        if nevs == 1: ymax =  nevs      * 1.5 + 0.5
        else:         ymax = (nevs + 1) * 1.5 + 0.5

        axis.set_ylim((ymax, -1))

        axis.set_xlabel('Seconds', va='bottom')
        axis.xaxis.set_label_coords(0.5, 10.0 / height)

        yticks  = list(0.5 + np.arange(nevs)  * 1.5)
        ylabels = feat.evnames[:]

        if nevs > 1:
            yticks  += [0.5 + nevs * 1.5]
            ylabels += ['Combined']

        xticks  = range(0, nsecs + 1, 30)
        xlabels = [f'{x}' for x in xticks]

        axis.set_xticks(xticks)
        axis.set_yticks(yticks)
        axis.set_xticklabels(xlabels)
        axis.set_yticklabels(ylabels)
        axis.grid()

        axis.tick_params(axis='both', which='both', length=3, direction='in')
        axis.tick_params(axis='y', pad=-5, rotation=90)
        axis.tick_params(axis='x', pad=-20)

        for ytl in axis.yaxis.get_ticklabels():
            ytl.set_verticalalignment('center')
            ytl.set_horizontalalignment('left')

        for lbl in axis.xaxis.get_ticklabels():
            lbl.set_verticalalignment('bottom')
            lbl.set_horizontalalignment('center')

        self.canvas.draw()


class EasyFeatPanel(wx.Panel):
    """EasyFEAT panel allowing a user to set up and run a FEAT analysis. """

    def __init__(self, parent, feat):
        wx.Panel.__init__(self, parent)
        self.feat = feat

        self.widgets = widgetlist.WidgetList(self)
        self.widgets.AddGroup('data',         'Data')
        self.widgets.AddGroup('pre-stats',    'Pre-stats')
        self.widgets.AddGroup('registration', 'Registration')
        self.widgets.AddGroup('experiment',   'Experiment')
        self.widgets.AddGroup('stats',        'Stats')
        self.widgets.AddGroup('post-stats',   'Post-stats')

        self.model = ModelPanel(self, feat)

        ######
        # Data
        ######

        self.widgets.AddWidget(
            props.buildGUI(self.widgets, feat, 'inputFile'),
            'Input file (4D time series image)',
            groupName='data')
        self.widgets.AddWidget(
            props.buildGUI(self.widgets, feat, 'outputDir'),
            'Output directory',
            groupName='data')

        self.trTime     = wx.StaticText(self.widgets)
        self.numVolumes = wx.StaticText(self.widgets)
        self.totalTime  = wx.StaticText(self.widgets)

        self.widgets.AddWidget(self.trTime,
                               'TR time (seconds)',
                               groupName='data')
        self.widgets.AddWidget(self.numVolumes,
                               'Number of volumes',
                               groupName='data')
        self.widgets.AddWidget(self.totalTime,
                               'Total time (seconds)',
                               groupName='data')

        ###########
        # Pre-stats
        ###########

        highPass       = props.buildGUI(self.widgets, feat, 'highPass')
        highPassCutoff = props.buildGUI(self.widgets, feat,
                                        props.Widget('highPassCutoff',
                                            enabledWhen=lambda o: o.highPass))

        hpfSizer = wx.BoxSizer(wx.HORIZONTAL)
        hpfSizer.Add(highPass,       flag=wx.EXPAND)
        hpfSizer.Add(highPassCutoff, flag=wx.EXPAND, proportion=1)

        self.widgets.AddWidget(hpfSizer,
                               'High pass filtering (seconds)',
                               groupName='pre-stats')

        smoothing     = props.buildGUI(self.widgets, feat, 'smoothing')
        smoothingSize = props.buildGUI(self.widgets, feat,
                                       props.Widget('smoothingSize',
                                           enabledWhen=lambda o: o.smoothing))

        smoSizer = wx.BoxSizer(wx.HORIZONTAL)
        smoSizer.Add(smoothing,     flag=wx.EXPAND)
        smoSizer.Add(smoothingSize, flag=wx.EXPAND, proportion=1)

        self.widgets.AddWidget(smoSizer,
                               'Spatial smoothing (mm)',
                               groupName='pre-stats')
        self.widgets.AddWidget(props.buildGUI(self.widgets, feat, 'motionCorrection'),
                               'Motion correction',
                               groupName='pre-stats')
        self.widgets.AddWidget(props.buildGUI(self.widgets, feat, 'brainExtraction'),
                               'Brain extraction',
                               groupName='pre-stats')

        ##############
        # Registration
        ##############

        self.widgets.AddWidget(props.buildGUI(self.widgets, feat, 'structural'),
                               'T1 structural image',
                               groupName='registration')
        self.widgets.AddWidget(props.buildGUI(self.widgets, feat, 'standard'),
                               'Standard space image',
                               groupName='registration')
        self.widgets.AddWidget(props.buildGUI(self.widgets, feat, 'standardDOF'),
                               'DOF for T1 to standard registration',
                               groupName='registration')

        ##################
        # Experiment+Stats
        ##################

        # EV/contrast widgets are managed in __nevsChanged
        self.widgets.AddWidget(
            props.buildGUI(self.widgets, feat, props.Widget('nevs',
                slider=False, showLimits=False)),
            '# conditions',
            groupName='experiment')

        ############
        # Post-stats
        ############

        self.widgets.AddWidget(props.buildGUI(
            self.widgets, feat, props.Widget(
                'thresholding',
                labels={0 : 'None',
                        1 : 'Uncorrected',
                        2 : 'Voxel',
                        3 : 'Cluster'})),
            'Thresholding type',
            groupName='post-stats')
        self.widgets.AddWidget(props.buildGUI(
            self.widgets, feat, props.Widget(
                'pThreshold',
                showLimits=False,
                slider=False,
                enabledWhen=lambda i: i.thresholding != 0)),
            'P threshold',
            groupName='post-stats')

        self.widgets.AddWidget(props.buildGUI(
            self.widgets, feat, props.Widget(
                'zThreshold',
                enabledWhen=lambda i: i.thresholding == 3)),
            'Z threshold',
            groupName='post-stats')

        # Buttons
        self.btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.btnSizer.Add((1, 1), flag=wx.EXPAND, proportion=2)

        self.btnSizer.Add(props.buildGUI(
            self, feat, props.Button(
                text='Run',
                callback=lambda i, ev: i.run())),
            flag=wx.EXPAND, proportion=1)
        self.btnSizer.Add((10, 1), flag=wx.EXPAND)
        self.btnSizer.Add(
            props.buildGUI(self, feat, props.Button(
                text='Exit',
                callback=lambda i, ev: sys.exit(0))),
            flag=wx.EXPAND, proportion=1)

        self.btnSizer.Add((20, 1), flag=wx.EXPAND)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.widgets,  flag=wx.EXPAND)
        self.sizer.Add(self.model,    flag=wx.EXPAND, proportion=1)
        self.sizer.Add(self.btnSizer, flag=wx.EXPAND | wx.ALL, border=5)
        self.SetSizer(self.sizer)
        self.Layout()

        self.widgets.Bind(widgetlist.EVT_WL_EXPAND_EVENT, self.__widgetsExpanded)

        feat.listen('nevs',       str(id(self)), self.__nevsChanged)
        feat.listen('trTime',     str(id(self)), self.__trTimeChanged)
        feat.listen('numVolumes', str(id(self)), self.__numVolumesChanged)

        self.__nevsChanged()
        self.__trTimeChanged()
        self.__numVolumesChanged()


    def __widgetsExpanded(self, ev):
        self.Layout()


    def __nevsChanged(self):
        """Manages widgets for EV configuration. """

        # The experiment section contains a widget for
        # nevs which we ignore, and a widget for each EV
        widgets    = self.widgets
        feat       = self.feat
        evwidgets  = widgets.GetWidgets('experiment')[1:]
        oldnevs    = len(evwidgets)
        nevs       = feat.nevs

        if nevs == oldnevs:
            return

        # nevs has been reduced
        if nevs < oldnevs:
            for w in evwidgets[nevs:]:
                widgets.RemoveWidget(w, 'experiment')

        # nevs has been increased
        elif nevs > oldnevs:
            for ev in range(len(evwidgets), nevs):

                name  = props.makeListWidget(widgets, feat, 'evnames',  ev)
                on    = props.makeListWidget(widgets, feat, 'evons',    ev, slider=False, showLimits=False)
                off   = props.makeListWidget(widgets, feat, 'evoffs',   ev, slider=False, showLimits=False)
                phase = props.makeListWidget(widgets, feat, 'evphases', ev, slider=False, showLimits=False)

                namelbl  = wx.StaticText(widgets, label='Name')
                onlbl    = wx.StaticText(widgets, label='On')
                offlbl   = wx.StaticText(widgets, label='Off')
                phaselbl = wx.StaticText(widgets, label='Phase')
                sizer    = wx.BoxSizer(wx.HORIZONTAL)

                sizer.Add((5, 5))
                sizer.Add(namelbl,  flag=wx.EXPAND)
                sizer.Add(name,     flag=wx.EXPAND, proportion=2)
                sizer.Add(offlbl,   flag=wx.EXPAND)
                sizer.Add(off,      flag=wx.EXPAND, proportion=1)
                sizer.Add(onlbl,    flag=wx.EXPAND)
                sizer.Add(on,       flag=wx.EXPAND, proportion=1)
                sizer.Add(phaselbl, flag=wx.EXPAND)
                sizer.Add(phase,    flag=wx.EXPAND, proportion=1)
                widgets.AddWidget(sizer, f'Condition {ev + 1}',
                                  groupName='experiment')

        # The stats section contains checkboxes
        # allowing the user to choose which
        # statistical tests (contrasts) to perform.
        widgets.ClearGroup('stats')

        # We have one row allowing the user to test
        # individual conditions, and another column
        # allowing them to compare conditions.
        testSizer    = wx.BoxSizer(wx.HORIZONTAL)
        compareSizer = wx.BoxSizer(wx.HORIZONTAL)

        testSizer   .Add((5, 5))
        compareSizer.Add((5, 5))

        # Test for mean across all EVs
        if nevs > 1:
            testmean = props.makeWidget(widgets, feat, 'meanContrast', cblabel='Mean')
            testSizer.Add(testmean, flag=wx.EXPAND, proportion=1)

        # Test for each EV individually
        for ev in range(nevs):
            testev = props.makeListWidget(widgets, feat, 'evContrasts', ev,
                                          cblabel=f'#{ev + 1}')
            testSizer.Add(testev, flag=wx.EXPAND, proportion=1)

        # Test for each pair of EVs
        evpairs = feat.evpairs

        if len(evpairs) == 0:
            na = wx.StaticText(widgets, label='N/A')
            compareSizer.Add(na, flag=wx.EXPAND, proportion=1)

        if len(evpairs) > 1:
            compall = props.makeWidget(widgets, feat, 'compareAll', cblabel='All')
            compareSizer.Add(compall, flag=wx.EXPAND, proportion=1)

        for i, (evx, evy) in enumerate(evpairs):
            compare = props.makeListWidget(widgets, feat, 'compareContrasts', i,
                                           cblabel=f'#{evx + 1} >/< #{evy + 1}')
            compareSizer.Add(compare, flag=wx.EXPAND, proportion=1)

        self.widgets.AddWidget(testSizer,    'Test conditions',    groupName='stats')
        self.widgets.AddWidget(compareSizer, 'Compare conditions', groupName='stats')


    def __trTimeChanged(self):
        nvols = self.feat.numVolumes
        tr    = self.feat.trTime
        self.trTime   .SetLabel(f' {tr:0.3f}')
        self.totalTime.SetLabel(f' {tr * nvols:0.1f}')


    def __numVolumesChanged(self):
        nvols = self.feat.numVolumes
        tr    = self.feat.trTime
        self.numVolumes.SetLabel(f' {nvols}')
        self.totalTime .SetLabel(f' {tr * nvols:0.1f}')


def main():

    if 'FSLDIR' not in os.environ:
        print('Cannot find FSL installation!')
        sys.exit(1)

    props.initGUI()

    feat  = EasyFeat()
    app   = wx.App()
    frame = wx.Frame(None, title='EasyFEAT')
    panel = EasyFeatPanel(frame, feat)

    panel.SetSize(panel.widgets.GetBestSize())

    sizer = wx.BoxSizer(wx.VERTICAL)
    sizer.Add(panel, flag=wx.EXPAND, proportion=1)
    frame.SetSizer(sizer)
    frame.SetMinSize((700, -1))
    frame.Show()
    frame.Layout()
    frame.Fit()
    frame.Refresh()

    app.MainLoop()


if __name__ == '__main__':
    main()
